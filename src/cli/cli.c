#include <string.h>
#include <stdio.h>
int mode=0;
int packagesLength=0;
//0 install
//1 remove
//2 update
//3 download
char cli_packages[50][50];
int cli(int argc, char* argv[]){
	if(strcmp(argv[1],"-i")==0){
		mode=0;
	}else if(strcmp(argv[1],"-r")==0){
		mode=1;
	}else if(strcmp(argv[1],"-u")==0){
		mode=2;
	}else if(strcmp(argv[1],"-d")==0){
		mode=3;
	}else{
		mode=-1;
	}
	for(int i=2;i<argc;i++){
		packagesLength++;
		strcpy(cli_packages[i-2],argv[i]);
	}
	return 0;
}
int cli_getMode(){
return mode;
}
int cli_getPackagesLength(){
	return packagesLength;
}
