#include <string.h>
char* replace_char(char* str, char find, char replace){
	char *current_pos = strchr(str,find);
	while (current_pos){
		*current_pos = replace;
		current_pos = strchr(current_pos,find);
	}
	return str;
}
char* safe_shell(char *name){
	return replace_char(replace_char(replace_char(replace_char(replace_char(name,'"',' '),'\'',' '),';',' '),'&',' '),'|',' ');
}

