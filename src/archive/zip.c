#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <util.h>


int zip(char* name,char* path){
	char cmd[500]="zip -9 -r ";
	strcat(cmd,safe_shell(name));
	strcat(cmd," ");
	strcat(cmd,safe_shell(path));
	return system(cmd);
}
	
int unzip(char* name,char* path){
	char cmd[500]="unzip -o ";
	strcat(cmd,safe_shell(name));
	strcat(cmd," -d ");
	strcat(cmd,safe_shell(path));
	return system(cmd);
}
