#include <package.h>
#include <stdlib.h>
package new_package(char* name){
    package new;
    new.name=name;
    new.deps=malloc(1024*sizeof(char*));
    new.dep_size=0;
    new.rel=1;
    return new;
}
int get_package_dep_size(package pkg){
    int i=0;
    while(pkg.deps[i]){
        i++;
    }
    return i;
}

package append_dep(package pkg,char*name){
    int size=pkg.dep_size;
    pkg.deps[size]=name;
    pkg.deps[size+1]='\0';
    pkg.dep_size=size+1;
    return pkg;
}
