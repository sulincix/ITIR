typedef struct {
    char* name;
    char** deps;
    int dep_size;
    int rel;
} package;
package new_package(char* name);
int get_package_dep_size(package);
package append_dep(package pkg,char*name);
