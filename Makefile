CC=gcc
CFLAGS=-O3 -s -fPIC
LINKS=-lcurl
#C files & includes
INCLUDES=`find src/ -type d | sed "s/src/-Isrc/g"`
CFILES=`find src/ -type f | grep ".c"`


all: clean build

clean:
	rm -rf bin
	mkdir bin || true
build:
	#Generate includes.h file
	find src | grep "\.h" | grep -v "includes.h" | sed "s|.*/|#include <|g" | sed "s/\.h/.h>/g" > src/includes.h
	echo "#include <stdio.h>" >> src/includes.h
	#Build libitir
	$(CC) $(CFLAGS) $(LINKS) -shared $(CFILES) $(INCLUDES) -o bin/libitir.so
	#Build itir
	$(CC) $(CFLAGS) $(INCLUDES) -litir -Lbin src/main.c -o bin/itir
install:
	: TODO add installation section
